 export const CONFIG = {
    app: {
        title: 'BROCK REST API',
        description: 'BROCK REST API - TYPESCRIPT NODEJS JWT',
        keywords: 'Nodejs, Typescript, JWT, MongoDB',
    },
    port: process.env.PORT || 3000,
    host: process.env.HOST || '0.0.0.0',
    // DOMAIN config should be set to the fully qualified application accessible
    domain: process.env.DOMAIN || 'https://api.freepowder.io',
    // Session Cookie settings
    sessionCookie: {
        // session expiration is set by default to 24 hours
        maxAge: 24 * (60 * 60 * 1000),
        // httpOnly flag makes sure the cookie is only accessed
        // through the HTTP protocol and not JS/browser
        httpOnly: true,
        // secure cookie should be turned to true to provide additional
        // layer of security so that the cookie is set only when working
        // in HTTPS mode.
        secure: false
    },
    jwtSecret: process.env.JWT_SECRET || '56bf407fec3arfs17876yveddde2ba09f90',
    // sessionSecret should be changed for security measures and concerns
    sessionSecret: process.env.SESSION_SECRET || '56bf407fec3arfs17876yveddde2ba09f90',
    // sessionKey is the cookie session name
    sessionKey: 'sessionId',
    sessionCollection: 'sessions',
    // Lusca config
    csrf: {
        csrf: false,
        csp: false,
        xframe: 'SAMEORIGIN',
        p3p: 'ABCDEF',
        xssProtection: true
    },
    logo: 'modules/core/brand/logo.png',
    favicon: 'modules/core/brand/logo.png',
    illegalUsernames: ['meanjs', 'administrator', 'password', 'admin', 'user',
        'unknown', 'anonymous', 'null', 'undefined', 'api', 'ecolove', 'arf'
    ],
    uploads: {
        profile: {
            image: {
                dest: './modules/users/server/uploads/',
                limits: {
                    fileSize: 1 * 1024 * 1024 // Max file size in bytes (1 MB)
                }
            }
        },
        article: {
            image: {
                dest: './modules/articles/server/uploads/',
                limits: {
                    fileSize: 1 * 1024 * 1024 // Max file size in bytes (1 MB)
                }
            }
        }
    },
    shared: {
        owasp: {
            allowPassphrases: true,
            maxLength: 20,
            minLength: 6,
            minPhraseLength: 20,
            minOptionalTestsToPass: 4
        }
    }

};
