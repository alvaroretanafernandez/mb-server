export const CONFIG = {
	app: {
		title: 'BROCK REST API',
		description: 'BROCK REST API - TYPESCRIPT NODEJS JWT',
		keywords: 'Nodejs, Typescript, JWT, MongoDB',
	},
	db: {
		uri: 'mongodb://localhost:27017' + '/brock-rest-api-dev',
	},
	port: process.env.PORT || 3000,
	host: process.env.HOST || '0.0.0.0',
	// DOMAIN config should be set to the fully qualified application accessible
};
