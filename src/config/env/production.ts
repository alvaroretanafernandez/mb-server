export const CONFIG = {
	secure: {
		ssl: true,
		privateKey: './config/sslcerts/key.pem',
		certificate: './config/sslcerts/cert.pem',
		caBundle: './config/sslcerts/cabundle.crt'
	},
	db: {
		uri: 'mongodb://localhost:27017' + '/brock-rest-api-prod',
	},
	port: process.env.PORT || 6000,
	host: process.env.HOST || '0.0.0.0',
	s3: {
		AWS_ACCESS_KEY: 'AKIAILUJAFFDUMT5IXDQ',
		AWS_SECRET_KEY: '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',
		AWS_REGION: 'eu-west-2',
		AWS_ENDPOINT: 's3.amazonaws.com',
		AWS_BUCKET: 'brock'
	},
	shared: {
		owasp: {
			allowPassphrases: true,
			maxLength: 20,
			minLength: 6,
			minPhraseLength: 20,
			minOptionalTestsToPass: 4
		}
	}
	
};
