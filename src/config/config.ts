import * as _ from 'lodash';
import * as path from 'path';
/**
 * node env
 * @type {string | undefined}
 */
const nodeEnv = process.env.NODE_ENV || 'development';

/**
 * Set path
 * @type {string}
 */
let envPath = nodeEnv === 'production' || nodeEnv === 'test' ? 'config/env/' :'src/config/env/';

 if (nodeEnv === 'test') {envPath = 'dist/config/env/'}
/**
 * Get default env config
 * @type {any}
 */
const defaultConfig = require(path.join(process.cwd(), envPath + 'default'));

/**
 * Get current env config
 * @type {{}}
 */
const environmentConfig = require(path.join(process.cwd(), envPath, nodeEnv)) || {};

/**
 * Merge
 */

export default _.merge(defaultConfig, environmentConfig).CONFIG;
