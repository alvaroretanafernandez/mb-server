
import AdminRoutes from '../modules/users/admin.routes';
import AuthRoutes from '../modules/users/auth.routes';
import CoreRoutes from '../modules/core/core.routes';
import UserRoutes from '../modules/users/users.routes';
import CmsRoutes from '../modules/cms/cms.routes';
import User from '../modules/users/model/user.model';
import Content from '../modules/cms/model/cms.content.model';
import Video from '../modules/cms/model/cms.video.model';
import Photo from '../modules/cms/model/cms.photo.model';
import Review from '../modules/cms/model/cms.review.model';
import AdminPolicy from '../modules/users/policies/admin.policy';
import CmsPolicy from '../modules/cms/policies/cms.policy';

export const MODELS_LIST = [
	User,
	Content,
	Video,
	Photo,
	Review
];
export const POLICIES_LIST = [
	AdminPolicy,
	CmsPolicy
];
export const ROUTES_LIST = [
	AuthRoutes,
	UserRoutes,
	AdminRoutes,
	CmsRoutes,
	// always the last one
	CoreRoutes,
];
