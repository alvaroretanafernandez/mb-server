import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as compress from 'compression';
import * as passport from 'passport';
import { NextFunction, Response, Request } from 'express';
import * as helmet from 'helmet';
import * as methodOverride from 'method-override';
import * as cookieParser from 'cookie-parser';
import * as expressJwt from 'express-jwt';
import * as cors from 'cors';
import APP_CONFIG from '../config/config';
import { ROUTES_LIST, MODELS_LIST, POLICIES_LIST } from './helper';
import User from '../modules/users/model/user.model';
import morgan = require('morgan');
import * as jwt from 'jsonwebtoken';
const nodeEnv = process.env.NODE_ENV || 'development';

class ExpressConfig {
	private app: express.Application;
	private SIX_MONTHS = 15778476000;
	private ROUTES = ROUTES_LIST;
	readonly MODELS = MODELS_LIST;
	private POLICIES = POLICIES_LIST;
	
	public init(app: express.Application) {
		
		this.app = app;
		this.initAppLocals();
		this.initJwt();
		this.initMiddleware();
		this.initPassport();
		this.initHelmetHeaders();
		this.initServerPolicies();
		this.initServerRoutes();
		this.initErrorRoutes();
	}
	
	private initAppLocals(): void {
		this.app.locals.name = 'Free Powder';
		this.app.locals.title = APP_CONFIG.app.title;
		this.app.locals.description = APP_CONFIG.app.description;
		if (APP_CONFIG.secure && APP_CONFIG.secure.ssl === true) {
			this.app.locals.secure = APP_CONFIG.secure.ssl;
		}
		this.app.locals.keywords = APP_CONFIG.app.keywords;
	}
	
	private initJwt(): void {
		
		const whitelist = [
			'http://localhost:4200',
			'https://brock.freepowder.io',
			'https://mattbrock.dicepeople.com',
			'https://dicepeople.com',
		];
		const corsOptions = {
			origin: function(origin, callback){
				var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
				callback(null, originIsWhitelisted);
			},
			credentials: true
		};
		this.app.use(cors(corsOptions));
		
		this.app.use('/api', expressJwt({ secret: APP_CONFIG.jwtSecret }).unless({
			path: [
				// public routes that don't require authentication
				'/api/cms/content/',
				'/api/auth/signup',
				'/api/auth/signin',
				'/api/auth/signout'
			]
		}));
		
		this.app.use((req, res, next) => {
			if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer'){
//				console.log(req.headers.authorization.split(' ')[1]);
				jwt.verify(req.headers.authorization.split(' ')[1], APP_CONFIG.jwtSecret
					,(err, decoded) =>{
						if (err) {
							res.status(401).json({
								site: 'https://api.freepowder.com/',
								error: {
									url: req.url,
									message: 'Invalid token',
									code: 401
								}
							});
						}
						next();
				});
			}else{
				next();
			}
		});
			
			
		this.app.use((err: Error, req: Request, res: Response, next: NextFunction): void => {
			if (!err) {
				return next();
			}
			
			if (err.name === 'UnauthorizedError') {
				res.status(401).json({
					site: 'https://api.freepowder.com/',
					error: {
						url: req.url,
						message: 'Invalid token',
						code: 401
					}
				});
			}
		});
		
	}
	
	private initMiddleware() {
		// compression
		this.app.use(compress({
			filter(req: Request, res: Response) {
				return (/json|text|javascript|css|font|svg/)
					.test(res.getHeader('Content-Type') ? res.getHeader('Content-Type').toString() : '');
			},
			level: 9
		}));
		// body & cookie parser
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.urlencoded({ extended: false }));
		this.app.use(methodOverride());
		this.app.use(cookieParser());
		if (nodeEnv === 'development') {
			this.app.use(morgan('tiny'));
		}
	}
	
	private initPassport(): void {
		// // serialize
		passport.serializeUser((user, done) => {
			return done(null, user['id']);
		});
		
		// Deserialize sessions
		passport.deserializeUser((id, done) => {
			User.findById({
				_id: id
			}, '-salt -password', (err, user) => {
				return done(err, user);
			});
		});
		//
		this.app.use(passport.initialize());
		this.app.use(passport.session());
	}
	
	private initHelmetHeaders(): void {
		
		this.app.use(helmet.frameguard());
		this.app.use(helmet.xssFilter());
		this.app.use(helmet.noSniff());
		this.app.use(helmet.ieNoOpen());
		this.app.use(helmet.hsts({
			maxAge: this.SIX_MONTHS,
			includeSubdomains: true,
			force: true
		}));
		this.app.disable('x-powered-by');
		
		
	}
	
	private initServerPolicies(): void {
		this.POLICIES.forEach((policy) => {
			policy.invokeRolesPolicies();
		});
	}
	
	private initServerRoutes(): void {
		this.ROUTES.forEach((routes) => {
			routes.mount(this.app);
		});
	}
	
	private initErrorRoutes(): void {
		this.app.use((err: Error, req: Request, res: Response, next: NextFunction): void => {
			if (!err) {
				return next();
			}
			console.log(err);
			res.redirect('/server-error');
		});
		
		this.app.use((req: Request, res: Response) => {
			res.status(404).send({
				site: 'https://api.freepowder.com/',
				error: {
					url: req.url,
					message: 'Not Found : These are not the droids you are looking for.',
					code: 404
				}
			});
		});
	}
	
}

export default new ExpressConfig();