import APP_CONFIG from '../config/config';
const mongoose = require('mongoose');

class ConnectToMongo {
	public connect(uri=APP_CONFIG.db.uri): Promise<any> {
		return new Promise((resolve, reject) => {
			mongoose.set('useCreateIndex', true);
			mongoose.connect(uri, { useNewUrlParser: true }).then(
				() => { resolve(); },
				err => {
					reject(err);
				}
			);;
			
		})
	}
}

export default new ConnectToMongo();