import 'mocha';
import app from '../App';
import ConnectToMongo from '../lib/mongoose';
import APP_CONFIG from '../config/config';
import * as http from 'http';

export default class CommonTests {
	
	private httpServer;
	
	public getTestApp = (): Promise <any> => {
		return new Promise((resolve, reject) => {
			ConnectToMongo.connect('mongodb://localhost:27017/FREEPOWDER-TEST')
				.then(() => {
					
					this.httpServer = http.createServer(app);
					
					this.httpServer.listen(APP_CONFIG.port, (err) => {
						if (err) {
							return console.log(err);
						}
						resolve(app);
						// return console.log(`server is listening on ${port}`);
					});
					
				})
		})
	};
	
	public closeTestApp = (): Promise <any> => {
		return new Promise((resolve, reject) => {
			this.httpServer.close();
			resolve();
		})
	}
};
