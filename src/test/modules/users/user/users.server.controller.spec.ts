import 'mocha';
import * as request from 'supertest';
import CommonTests from '../../../common.tests';
import UserServerController from '../../../../modules/users/controllers/user.server.controller';
const mongoose = require('mongoose');
const expect = require('expect.js');

let _app, _agent, _conn, _users, _ct;

describe('Users Module - User Controller', () => {
	
	before( (done) => {
		// Get application
		_ct = new CommonTests();
		_ct.getTestApp().then((app) => {
			_app = app;
			_agent = request.agent(app);
			_users = UserServerController;
			done();
		});
		
	});
	it('should have a method to get user', (done) => {
		expect(_users.me).to.be.ok();
		done();
	});
	
	it('should have a method to update user', (done) => {
		expect(_users.update).to.be.ok();
		done();
	});
	
	it('should have a method to change user password', (done) => {
		expect(_users.changePassword).to.be.ok();
		done();
	});
	
	after((done) => {
		_conn = mongoose.connection;
		_conn.close();
		_ct.closeTestApp().then(() => {
			done();
		});
	});
});