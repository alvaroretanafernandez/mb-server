const request = require('supertest');
import CommonTests from '../../../common.tests';
const mongoose = require('mongoose');
const expect = require('expect.js');
import User from '../../../../modules/users/model/user.model';


let _app, _agent, _conn, _ct, _credentialsEmail, _user, _testUser;

describe('Users Module - Admin Routes', () => {
		before((done) => {
			// Get application
			_ct = new CommonTests();
			_ct.getTestApp().then((app) => {
				_app = app;
				_agent = request(app);
				_credentialsEmail = { email: 'test@email.com', password: 'Pass001' };
				_user = {
					email: _credentialsEmail.email,
					password: _credentialsEmail.password,
					provider: 'local',
				};
				_testUser = new User(_user);
				done();
			})
		});
		
		it('should be able to retrieve a list of users if admin : /api/users', (done) => {
			//_credentialsEmail = { email: 'test2@email.com', password: 'Pass001' };
			_testUser.roles = ['user', 'admin'];
			_testUser.save()
				.then((us) => {
					expect(us).to.be.ok();
					_agent.post('/api/auth/signin')
						.send(_credentialsEmail)
						.expect(200)
						.end((err, resp) => {
							expect(resp.body.user).to.be.ok();
							expect(resp.body.token).to.be.ok();
							if (err) {
								done(err);
							}
							// get Users
							_agent.get('/api/users')
								.set('authorization', 'Bearer ' + resp.body.token)
								.expect(200)
								.end((usErr, usRes) => {
									if (usErr) {
										return done(usErr);
									}
									expect(usRes.body).to.be.ok();
									return done();
								});
						});
				})
				.catch((err) => {
					done(err);
				});
			
		});
		
		it('should get 401 when try to retrieve a list of users if not admin : /api/users', (done) => {
			_agent.post('/api/users')
				.expect(401)
				.end((err, resp) => {
					// Call the assertion callback
					expect(resp.body.error.message).to.be('Invalid token');
					done(err);
				});
		});
		
		after((done) => {
			_conn = mongoose.connection;
			_conn.db.dropCollection('users', () => {
				_conn.close();
				_ct.closeTestApp().then(() => {
					done();
				});
			});
		});
});
