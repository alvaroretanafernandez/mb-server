import * as mocha from 'mocha';
const request = require('supertest');
import CommonTests from '../../../common.tests';
import AuthServerController from '../../../../modules/users/controllers/auth.server.controller';
const mongoose = require('mongoose');
const expect = require('expect.js');
import User from '../../../../modules/users/model/user.model';


let _app, _agent, _conn, _auth, _ct, _credentialsEmail, _user, _testUser;

describe('Users Module - Auth Routes', () => {
		before((done) => {
			// Get application
			_ct = new CommonTests();
			_ct.getTestApp().then((app) => {
				_app = app;
				_agent = request(app);
				_auth = AuthServerController;
				_credentialsEmail = { email: 'test@email.com', password: 'Pass001' };
				_user = {
					email: _credentialsEmail.email,
					password: _credentialsEmail.password,
					provider: 'local',
				};
				_testUser = new User(_user);
				_testUser.save()
					.then((us) => {
						expect(us).to.be.ok();
						done();
					}).catch((err) => {
					done(err);
				});
			})
		});
		it('should be able to register new users : /api/auth/signup', (done) => {
			_credentialsEmail.email = 'clean@email.com';
			_agent.post('/api/auth/signup')
				.send(_credentialsEmail)
				.expect(200)
				.end((err, resp) => {
					// Call the assertion callback
					expect(resp.body.user).to.be.ok();
					expect(resp.body.token).to.be.ok();
					done(err);
				});
		});
		it('should be able to not register new users : /api/auth/signup', (done) => {
			_credentialsEmail = { email: 'test@email.com', password: 'Pass001' };
			_agent.post('/api/auth/signup')
				.send(_credentialsEmail)
				.expect(200)
				.end((err, resp) => {
					// Call the assertion callback
					expect(resp.body.user).to.be.ok();
					expect(resp.body.token).to.be.ok();
					done(err);
				});
		});
		it('should be able to login existing users : /api/auth/signin', (done) => {
			_agent.post('/api/auth/signin')
				.send(_credentialsEmail)
				.expect(200)
				.end((err, resp) => {
					expect(resp.body.user).to.be.ok();
					expect(resp.body.token).to.be.ok();
					done(err);
				});
		});
		it('should be able to logout existing logged in users : api/auth/signout', (done) => {
			_agent.post('/api/auth/signin')
				.send(_credentialsEmail)
				.expect(200)
				.end((err, resp) => {
					expect(resp.body.user).to.be.ok();
					expect(resp.body.token).to.be.ok();
					if (err) {
						done(err);
					}
					// Logout
					_agent.get('/api/auth/signout')
						.expect(200)
						.end((signoutErr, signoutRes) => {
							if (signoutErr) {
								return done(signoutErr);
							}
							expect(signoutRes.body.user).to.be(null);
							return done();
						});
				});
		});
		after((done) => {
			_conn = mongoose.connection;
			_conn.db.dropCollection('users', () => {
				_conn.close();
				_ct.closeTestApp().then(() => {
					done();
				});
			});
		});
});
