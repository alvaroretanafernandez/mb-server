import 'mocha';
import * as request from 'supertest';
import CommonTests from '../../../common.tests';
import CmsContentServerController from '../../../../modules/cms/controllers/cms.content.server.controller';
const mongoose = require('mongoose');
const expect = require('expect.js');

let _app, _agent, _conn, _content, _ct;

describe('CMS Module - Content Controller', () => {
	
	before( (done) => {
		// Get application
		_ct = new CommonTests();
		_ct.getTestApp().then((app) => {
			_app = app;
			_agent = request.agent(app);
			_content = CmsContentServerController;
			done();
		});
	});
	it('should have a method to get list of content', (done) => {
		expect(_content.list).to.be.ok();
		done();
	});
	it('should have a method to get content', (done) => {
		expect(_content.read).to.be.ok();
		done();
	});
	it('should have a method to create content', (done) => {
		expect(_content.create).to.be.ok();
		done();
	});
	it('should have a method to update content', (done) => {
		expect(_content.update).to.be.ok();
		done();
	});
	
	it('should have a method to delete content', (done) => {
		expect(_content.delete).to.be.ok();
		done();
	});
	
	after((done) => {
		_conn = mongoose.connection;
		_conn.close();
		_ct.closeTestApp().then(() => {
			done();
		});
	});
});