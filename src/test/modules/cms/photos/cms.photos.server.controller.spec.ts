import 'mocha';
import * as request from 'supertest';
import CommonTests from '../../../common.tests';
import CmsPhotosServerController from '../../../../modules/cms/controllers/cms.photos.server.controller';
const mongoose = require('mongoose');
const expect = require('expect.js');

let _app, _agent, _conn, _content, _ct;

describe('CMS Module - Photos Controller', () => {
	
	before( (done) => {
		// Get application
		_ct = new CommonTests();
		_ct.getTestApp().then((app) => {
			_app = app;
			_agent = request.agent(app);
			_content = CmsPhotosServerController;
			done();
		});
	});
	it('should have a method to get list of photos', (done) => {
		expect(_content.list).to.be.ok();
		done();
	});
	it('should have a method to get photos', (done) => {
		expect(_content.read).to.be.ok();
		done();
	});
	it('should have a method to create photos', (done) => {
		expect(_content.create).to.be.ok();
		done();
	});
	it('should have a method to update photos', (done) => {
		expect(_content.update).to.be.ok();
		done();
	});
	
	it('should have a method to delete photos', (done) => {
		expect(_content.delete).to.be.ok();
		done();
	});
	
	after((done) => {
		_conn = mongoose.connection;
		_conn.close();
		_ct.closeTestApp().then(() => {
			done();
		});
	});
});