
import * as mongoose from 'mongoose';

const PhotoSchema = new mongoose.Schema({
	image: {
		type: String
	},
	description: {
		type: String
	},
	title: {
		type: String,
	},
	type: {
		type: String,
	},
	isActive: {
		type: Boolean,
	},
	updated: {
		type: Date
	},
	created: {
		type: Date,
		default: Date.now
	}
});
export default mongoose.model( 'Photo', PhotoSchema, 'photos');