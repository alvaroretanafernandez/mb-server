
import * as mongoose from 'mongoose';

const VideoSchema = new mongoose.Schema({
	videoId: {
		type: String
	},
	iframe: {
		type: String
	},
	pic: {
		type: String
	},
	title: {
		type: String,
	},
	type: {
		type: String,
	},
	isActive: {
		type: Boolean,
	},
	updated: {
		type: Date
	},
	created: {
		type: Date,
		default: Date.now
	}
});
export default mongoose.model( 'Video', VideoSchema, 'videos');