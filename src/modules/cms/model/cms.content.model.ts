
import * as mongoose from 'mongoose';

const ContentSchema = new mongoose.Schema({
	firstName: {
		type: String
	},
	lastName: {
		type: String
	},
	otherName: {
		type: String
	},
	title: {
		type: String,
	},
	bio: {
		type: String,
	},
	videos: [
		{ type: mongoose.Schema.Types.ObjectId, ref: 'Video' }
	],
	photos: [
		{ type: mongoose.Schema.Types.ObjectId, ref: 'Photo' }
	],
	reviews: [
		{ type: mongoose.Schema.Types.ObjectId, ref: 'Review' }
	],
	isActive: {
		type: Boolean,
	},
	updated: {
		type: Date
	},
	created: {
		type: Date,
		default: Date.now
	}
});
export default mongoose.model( 'Content', ContentSchema, 'contents');