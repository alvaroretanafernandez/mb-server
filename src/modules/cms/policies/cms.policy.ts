import * as acll from 'acl';
import { NextFunction, Request, Response } from 'express';
const acl = new acll(new acll.memoryBackend);

class CmsPolicy {
	public invokeRolesPolicies = () => {
		acl.allow([
			{
				roles: ['admin'],
				allows: [
					{
						resources: ['/api/cms/content', '/api/cms/content/:contentId'],
						permissions: '*'
					},
					{
						resources: ['/api/cms/videos', '/api/cms/videos/:videoId'],
						permissions: '*'
					},
					{
						resources: ['/api/cms/photos', '/api/cms/photos/:photoId'],
						permissions: '*'
					},
					{
						resources: ['/api/cms/reviews', '/api/cms/reviews/:reviewId'],
						permissions: '*'
					}
				]
			},
			{
				roles: ['user'],
				allows: [
					{
						resources: ['/api/cms/content'],
						permissions: ['get']
					},
					{
						resources: ['/api/cms/content/:contentId'],
						permissions: ['get','put']
					},
					{
						resources: ['/api/cms/videos', '/api/cms/videos/:videoId'],
						permissions: ['get', 'put', 'post', 'delete']
					},
					{
						resources: ['/api/cms/photos', '/api/cms/photos/:photoId'],
						permissions: ['get', 'put', 'post', 'delete']
					},
					{
						resources: ['/api/cms/reviews', '/api/cms/reviews/:reviewId'],
						permissions: ['get', 'put', 'post', 'delete']
					}
				]
			},
			{
				roles: ['guest'],
				allows: [
					{
						resources: ['/api/cms/content', '/api/cms/content/:contentId'],
						permissions: ['get']
					},
					{
						resources: ['/api/cms/videos', '/api/cms/videos/:videoId'],
						permissions: ['get']
					}
				]
			}
		]);
	};
	
	public isAllowed = (req: Request, res: Response, next: NextFunction) => {
		const roles = (req.user) ? req.user.roles : ['guest'];
		// Check for user roles
		acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase()).then((isAllowed) => {
			
			
			if (isAllowed) {
				// Access granted! Invoke next middleware
				return next();
			} else {
				return res.status(403).json({
					message: 'User is not authorized'
				});
			}
		}).catch((err) => {
			return res.status(500).send('Unexpected authorization error');
		});
	}
}

export default new CmsPolicy();