import { NextFunction, Request, Response } from 'express';
import Photo from '../model/cms.photo.model';
import * as mongoose from 'mongoose';
import * as _ from 'lodash';


class CmsPhotosServerController {
	
	public list = (req: Request, res: Response): void => {
		Photo.find({})
			.then((photo) => {
				res.status(200).json(photo);
			})
			.catch((err) => {
				return res.status(422).send({message: err});
			});
	};
	
	public create = (req: Request, res: Response): void => {
		const _content = new Photo(req.body);
		_content.save()
			.then((photo) => {
				res.status(200).json(photo);
			})
			.catch((err) => {
				console.log(err);
				return res.status(422).send(err);
			})
	};
	
	
	
	public read = (req: Request, res: Response): void => {
		Photo.findById(req['photo']._id)
			.then((photo) => {
				res.status(200).json(photo);
			})
			.catch((err) => {
				res.status(422).send(err);
			});
		
	};
	
	
	public update = (req: Request, res: Response): void => {
		
		Photo.findById(req['photo']._id)
			.then((photo) => {
				photo = _.extend(photo, req.body);
				photo['updated'] = Date.now();
				photo.save()
					.then((_content) => {
						res.status(200).json(_content);
					})
					.catch((err) => {
						return res.status(422).send({
							message: err
						});
					});
			})
			.catch((err) => {
				res.status(422).send(err);
			});
	};
	
	public delete = (req: Request, res: Response): void => {
		const photo = req['photo'];
		photo.remove()
			.then(() => {
				res.status(200).json(photo);
			})
			.catch((err) => {
				return res.status(422).send({
					message: err
				});
			});
		
	};
	
	
	public photoByID = (req: Request, res: Response, next: NextFunction, id) => {
		
		if (!mongoose.Types.ObjectId.isValid(id)) {
			return res.status(400).send({
				message: 'photo is invalid'
			});
		}
		
		Photo.findById(id)
			.then((content) => {
				if (!content) {
					return next(new Error('Failed to load content ' + id));
				}
				req['photo'] = content;
				next();
			})
			.catch((err) => {
				console.log(err);
				return next(err);
			})
			
	};
}

export default new CmsPhotosServerController();