import { NextFunction, Request, Response } from 'express';
import Review from '../model/cms.review.model';
import * as mongoose from 'mongoose';
import * as _ from 'lodash';


class CmsReviewsServerController {
	
	public list = (req: Request, res: Response): void => {
		Review.find({})
			.then((reviews) => {
				res.status(200).json(reviews);
			})
			.catch((err) => {
				return res.status(422).send({message: err});
			});
	};
	
	public create = (req: Request, res: Response): void => {
		const _content = new Review(req.body);
		_content.save()
			.then((review) => {
				res.status(200).json(review);
			})
			.catch((err) => {
				console.log(err);
				return res.status(422).send(err);
			})
	};
	
	
	
	public read = (req: Request, res: Response): void => {
		Review.findById(req['review']._id)
			.then((review) => {
				res.status(200).json(review);
			})
			.catch((err) => {
				res.status(422).send(err);
			});
		
	};
	
	
	public update = (req: Request, res: Response): void => {
		
		Review.findById(req['review']._id)
			.then((review) => {
				review = _.extend(review, req.body);
				review['updated'] = Date.now();
				review.save()
					.then((_content) => {
						res.status(200).json(_content);
					})
					.catch((err) => {
						return res.status(422).send({
							message: err
						});
					});
			})
			.catch((err) => {
				res.status(422).send(err);
			});
	};
	
	public delete = (req: Request, res: Response): void => {
		const review = req['review'];
		review.remove()
			.then(() => {
				res.status(200).json(review);
			})
			.catch((err) => {
				return res.status(422).send({
					message: err
				});
			});
		
	};
	
	
	public reviewByID = (req: Request, res: Response, next: NextFunction, id) => {
		
		if (!mongoose.Types.ObjectId.isValid(id)) {
			return res.status(400).send({
				message: 'review is invalid'
			});
		}
		
		Review.findById(id)
			.then((content) => {
				if (!content) {
					return next(new Error('Failed to load review ' + id));
				}
				req['review'] = content;
				next();
			})
			.catch((err) => {
				console.log(err);
				return next(err);
			})
			
	};
}

export default new CmsReviewsServerController();