import { NextFunction, Request, Response } from 'express';
import Video from '../model/cms.video.model';
import * as mongoose from 'mongoose';
import * as _ from 'lodash';


class CmsVideosServerController {
	
	public list = (req: Request, res: Response): void => {
		Video.find({})
			.then((video) => {
				res.status(200).json(video);
			})
			.catch((err) => {
				return res.status(422).send({message: err});
			});
	};
	
	public create = (req: Request, res: Response): void => {
		const _content = new Video(req.body);
		_content.save()
			.then((video) => {
				res.status(200).json(video);
			})
			.catch((err) => {
				console.log(err);
				return res.status(422).send(err);
			})
	};
	
	
	
	public read = (req: Request, res: Response): void => {
		Video.findById(req['video']._id)
			.then((video) => {
				res.status(200).json(video);
			})
			.catch((err) => {
				res.status(422).send(err);
			});
		
	};
	
	
	public update = (req: Request, res: Response): void => {
		
		Video.findById(req['video']._id)
			.then((video) => {
				video = _.extend(video, req.body);
				video['updated'] = Date.now();
				video.save()
					.then((_content) => {
						res.status(200).json(_content);
					})
					.catch((err) => {
						return res.status(422).send({
							message: err
						});
					});
			})
			.catch((err) => {
				res.status(422).send(err);
			});
	};
	
	public delete = (req: Request, res: Response): void => {
		var video = req['video'];
		
		video.remove()
			.then(() => {
				res.status(200).json(video);
			})
			.catch((err) => {
				return res.status(422).send({
					message: err
				});
			});
		
	};
	
	
	public videoByID = (req: Request, res: Response, next: NextFunction, id) => {
		
		if (!mongoose.Types.ObjectId.isValid(id)) {
			return res.status(400).send({
				message: 'video is invalid'
			});
		}
		
		Video.findById(id)
			.then((content) => {
				if (!content) {
					return next(new Error('Failed to load content ' + id));
				}
				req['video'] = content;
				next();
			})
			.catch((err) => {
				console.log(err);
				return next(err);
			})
			
	};
}

export default new CmsVideosServerController();