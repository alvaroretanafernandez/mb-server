import app from './App';
import chalk from 'chalk';
import ConnectToMongo from './lib/mongoose';
import APP_CONFIG  from './config/config';
import * as fs from 'fs';
import * as path from 'path';
import * as https from 'https';
import * as http from 'http';

const nodeEnv = process.env.NODE_ENV || 'development';
const isProd = nodeEnv === 'production';
const port = process.env.PORT || APP_CONFIG.port;

let _server;

class StartExpress {
	
	_server;
	
	public init() {
		
		ConnectToMongo.connect()
			.then(() => {
				this.createServer();
			})
			.catch((err) => {
				console.log(chalk.blue('============================================================='));
				console.log(chalk.blue(APP_CONFIG.app.title));
				console.log(chalk.blue('============================================================='));
				console.log(chalk.red('[ERROR] :     ...Could not connect to MongoDB!'));
				console.log(chalk.red(err));
				console.log(chalk.blue('============================================================='));
			});
		
	}
	
	private createServer() {
		if (APP_CONFIG.secure && APP_CONFIG.secure.ssl === true) {
			const options = this.getSecureOptions();
			// Create new HTTPS Server
			_server =  https.createServer(options, app);
		} else {
			// Create a new HTTP server
			_server = http.createServer(app);
		}
		
		_server.listen(port, (err) => {
			if (err) {
				return console.log(err);
			}
			const server = (isProd ? 'https://localhost' : 'http://localhost') + ':' + port;
			console.log(chalk.blue('============================================================='));
			console.log(chalk.blue(APP_CONFIG.app.title));
			console.log(chalk.blue('============================================================='));
			console.log(chalk.yellow('Environment:     ' + nodeEnv));
			console.log(chalk.magenta('Server:          ' + server));
			console.log(chalk.green('Database:        ' + APP_CONFIG.db.uri));
			console.log(chalk.gray('App version:     ' + '0.0.1'));
			console.log(chalk.blue('============================================================='));
			return;
		});
	}
	
	private getSecureOptions(){
		// Load SSL key and certificate
		const privateKey = fs.readFileSync(path.resolve(APP_CONFIG.secure.privateKey), 'utf8');
		const certificate = fs.readFileSync(path.resolve(APP_CONFIG.secure.certificate), 'utf8');
		let caBundle;
		
		try {
			caBundle = fs.readFileSync(path.resolve(APP_CONFIG.secure.caBundle), 'utf8');
		} catch (err) {
			console.log('Warning: couldn\'t find or read caBundle file');
		}
		return{
			key: privateKey,
			cert: certificate,
			ca: caBundle,
			//  requestCert : true,
			//  rejectUnauthorized : true,
			secureProtocol: 'TLSv1_method',
			ciphers: [
				'ECDHE-RSA-AES128-GCM-SHA256',
				'ECDHE-ECDSA-AES128-GCM-SHA256',
				'ECDHE-RSA-AES256-GCM-SHA384',
				'ECDHE-ECDSA-AES256-GCM-SHA384',
				'DHE-RSA-AES128-GCM-SHA256',
				'ECDHE-RSA-AES128-SHA256',
				'DHE-RSA-AES128-SHA256',
				'ECDHE-RSA-AES256-SHA384',
				'DHE-RSA-AES256-SHA384',
				'ECDHE-RSA-AES256-SHA256',
				'DHE-RSA-AES256-SHA256',
				'HIGH',
				'!aNULL',
				'!eNULL',
				'!EXPORT',
				'!DES',
				'!RC4',
				'!MD5',
				'!PSK',
				'!SRP',
				'!CAMELLIA'
			].join(':'),
			honorCipherOrder: true
		};
	}
}


new StartExpress().init();


